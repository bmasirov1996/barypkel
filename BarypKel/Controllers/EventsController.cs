﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BarypKel.Data.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using BarypKel.Domain.IRepository;
using System;
using System.IO;

namespace BarypKel.Controllers
{
    public class EventsController : Controller
    {
        private readonly IEventRepository _eventRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public EventsController(IEventRepository eventRepository, IWebHostEnvironment webHostEnvironment)
        {
            _eventRepository = eventRepository;
            _webHostEnvironment = webHostEnvironment;
        }

       
        public async Task<IActionResult> Index()
        {
            return View(await _eventRepository.ListAllAsync());
        }

        
        public async Task<IActionResult> Details(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var @event = await _eventRepository.GetByIdAsync(id);
            if (@event == null)
            {
                return NotFound();
            }

            return View(@event);
        }

        
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,EventName,Description,Guests,EventDate")] Event @event, IFormFile eventImage)
        {
            if (ModelState.IsValid)
            {
                if (eventImage != null)
                {
                    string folderPath = Path.Combine(_webHostEnvironment.WebRootPath, "images", "Event_images");
                    var filename = UploadImage(eventImage, folderPath);

                    @event.ImageName = filename;
                }
                await _eventRepository.AddAsync(@event);
                return RedirectToAction(nameof(Index));
            }
            return View(@event);
        }

        [Authorize]
        public async Task<IActionResult> Edit(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var @event = await _eventRepository.GetByIdAsync(id);
            if (@event == null)
            {
                return NotFound();
            }
            return View(@event);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,EventName,Desctiption,Guests,EventDate")] Event @event)
        {
            if (id != @event.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                 await   _eventRepository.UpdateAsync(@event);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EventExists(@event.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(@event);
        }

        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var @event = await _eventRepository.GetByIdAsync(id);
            if (@event == null)
            {
                return NotFound();
            }

            return View(@event);
        }

        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var @event = await _eventRepository.GetByIdAsync(id);
            _eventRepository.Delete(@event);
            return RedirectToAction(nameof(Index));
        }

        private bool EventExists(int id)
        {
            var @event =   _eventRepository.GetById(id);
            return @event != null;
        }

        private  string UploadImage(IFormFile image, string folderpath)
        {
            string fileName = $"{Guid.NewGuid().ToString()}_{image.FileName}";
            string filePath = Path.Combine(folderpath, fileName);

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                image.CopyTo(stream);
            }
            return fileName;
        }
    }
}
