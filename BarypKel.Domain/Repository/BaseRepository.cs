﻿using BarypKel.Data.Entities;
using BarypKel.Domain.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BarypKel.Domain.Repository
{
   public class BaseRepository<T> : IRepository<T>, IAsyncRepository<T> where T : Entity
    {
        protected readonly Context Context;

        public BaseRepository(Context context)
        {
            Context = context;
        }

        public virtual T GetById(int id)
        {
            return Context.Set<T>().Find(id);
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await Context.Set<T>().FindAsync(id);
        }

        public virtual IList<T> ListAll()
        {
            return Context.Set<T>().ToList();
        }

        public async Task<IList<T>> ListAllAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }

        public T Add(T entity)
        {
            Context.Set<T>().Add(entity);
            Context.SaveChanges();

            return entity;
        }

        public async Task<T> AddAsync(T entity)
        {
            Context.Set<T>().Add(entity);
            await Context.SaveChangesAsync();

            return entity;
        }

        public void Update(T entity)
        {

            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public async Task UpdateAsync(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            await Context.SaveChangesAsync();
        }

        public void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
            Context.SaveChanges();
        }

        public async Task DeleteAsync(T entity)
        {
            Context.Set<T>().Remove(entity);
            await Context.SaveChangesAsync();
        }

    }
}
