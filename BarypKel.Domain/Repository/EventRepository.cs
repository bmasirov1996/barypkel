﻿using BarypKel.Data.Entities;
using BarypKel.Domain.IRepository;
 

namespace BarypKel.Domain.Repository
{
    public class EventRepository : BaseRepository<Event>, IEventRepository
    {
        public EventRepository(Context context) : base(context)
        {
        }
    }
}
