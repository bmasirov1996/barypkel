﻿using BarypKel.Data.Entities;

namespace BarypKel.Domain.IRepository
{
    public interface IEventRepository : IRepository<Event>, IAsyncRepository<Event>
    {

    }
}
