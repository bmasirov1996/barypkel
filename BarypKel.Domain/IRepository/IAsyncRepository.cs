﻿using BarypKel.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BarypKel.Domain.IRepository
{
    public interface IAsyncRepository<T> where T : Entity
    {
        Task<T> GetByIdAsync(int id);
        Task<IList<T>> ListAllAsync();
        Task<T> AddAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
    }
}
