﻿using BarypKel.Data.Entities;
using System.Collections.Generic;

namespace BarypKel.Domain
{
    public interface IRepository<T> where T : Entity
    {
        T GetById(int id);
        IList<T> ListAll();
        T Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
