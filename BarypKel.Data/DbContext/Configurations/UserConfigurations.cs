﻿using BarypKel.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace BarypKel.Data.DbContext.Configurations
{
    public class UserConfigurations : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("t_user");
            builder.Property(x => x.Id).HasColumnName("id");
            builder.Property(x => x.Email).HasColumnName("user_email");
            builder.Property(x => x.PhoneNumber).HasColumnName("phone_number");
            builder.HasMany(x => x.Tickets).WithOne(c => c.User).HasForeignKey(x => x.Id);
        }
    }
}
