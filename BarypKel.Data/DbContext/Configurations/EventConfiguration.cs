﻿using BarypKel.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BarypKel.Data.DbContext.Configurations
{
    public class EventConfiguration : IEntityTypeConfiguration<Event>
    {
        public void Configure(EntityTypeBuilder<Event> builder)
        {
            builder.ToTable("t_event");
            builder.Property(x => x.Id).HasColumnName("id");
            builder.Property(x => x.EventDate).HasColumnName("date");
            builder.Property(x => x.EventName).HasColumnName("event_name");
            builder.Property(x => x.Description).HasColumnName("description");
            builder.Property(x => x.Guests).HasColumnName("guests");
            builder.HasMany(x => x.Tickets).WithOne(c => c.Event).HasForeignKey(x => x.Id);
        }
    }
}
