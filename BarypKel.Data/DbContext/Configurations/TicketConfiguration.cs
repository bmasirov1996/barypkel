﻿using BarypKel.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BarypKel.Data.DbContext.Configurations
{
    class TicketConfiguration : IEntityTypeConfiguration<Ticket>
    {
        public void Configure(EntityTypeBuilder<Ticket> builder)
        {
            builder.ToTable("t_ticket");
            builder.Property(x => x.Id).HasColumnName("id");
            builder.Property(x => x.EventDate).HasColumnName("date");
            builder.Property(x => x.QRCodePath).HasColumnName("qr_code");
            builder.HasOne(x => x.Event).WithMany(c => c.Tickets); ;
            builder.HasOne(x => x.User).WithMany(c => c.Tickets).HasForeignKey(x => x.Id);
        }
    }
}
