﻿using BarypKel.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BarypKel.Data.DbContext.Configurations
{
    public  class AdminConfiguration : IEntityTypeConfiguration<Admin>
    {
        public void Configure(EntityTypeBuilder<Admin> builder)
        {
            builder.ToTable("t_admin");
            builder.Property(x => x.Id).HasColumnName("id");
            builder.Property(x => x.Email).HasColumnName("login");
            builder.Property(x => x.PasswordHash).HasColumnName("password");
            builder.Property(x => x.LastLoginDate).HasColumnName("login_date");
        }
    }
}
