﻿using BarypKel.Data.DbContext.Configurations;
using BarypKel.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;


namespace BarypKel
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {
             
        }

        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<IdentityUserClaim<string>> IdentityUserClaim { get; set; }
        public DbSet<IdentityUserRole<string>> IdentityUserRole { get; set; }
        public DbSet<IdentityRole> IdentityRole { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            new UserConfigurations().Configure(modelBuilder.Entity<User>());
            new TicketConfiguration().Configure(modelBuilder.Entity<Ticket>());
            new EventConfiguration().Configure(modelBuilder.Entity<Event>());
            new AdminConfiguration().Configure(modelBuilder.Entity<Admin>());
            modelBuilder.Entity<IdentityUserRole<string>>().HasKey(p => new { p.UserId });
            base.OnModelCreating(modelBuilder);
        }
    }
}
