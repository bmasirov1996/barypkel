START TRANSACTION;

ALTER TABLE `t_ticket` DROP COLUMN `event_name`;

ALTER TABLE `t_ticket` ADD `EventId` int NOT NULL DEFAULT 0;

CREATE TABLE `t_event` (
    `id` int NOT NULL AUTO_INCREMENT,
    `event_name` longtext CHARACTER SET utf8mb4 NULL,
    `description` longtext CHARACTER SET utf8mb4 NULL,
    `guests` longtext CHARACTER SET utf8mb4 NULL,
    `date` datetime(6) NOT NULL,
    CONSTRAINT `PK_t_event` PRIMARY KEY (`id`)
) CHARACTER SET utf8mb4;

ALTER TABLE `t_ticket` ADD CONSTRAINT `FK_t_ticket_t_event_id` FOREIGN KEY (`id`) REFERENCES `t_event` (`id`) ON DELETE CASCADE;

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20210731072752_AddEntityEvent', '5.0.8');

COMMIT;

