CREATE TABLE IF NOT EXISTS `__EFMigrationsHistory` (
    `MigrationId` varchar(150) CHARACTER SET utf8mb4 NOT NULL,
    `ProductVersion` varchar(32) CHARACTER SET utf8mb4 NOT NULL,
    CONSTRAINT `PK___EFMigrationsHistory` PRIMARY KEY (`MigrationId`)
) CHARACTER SET utf8mb4;

START TRANSACTION;

ALTER DATABASE CHARACTER SET utf8mb4;

CREATE TABLE `t_user` (
    `id` int NOT NULL AUTO_INCREMENT,
    `user_email` longtext CHARACTER SET utf8mb4 NULL,
    `phone_number` longtext CHARACTER SET utf8mb4 NULL,
    `OrderDate` datetime(6) NOT NULL,
    CONSTRAINT `PK_t_user` PRIMARY KEY (`id`)
) CHARACTER SET utf8mb4;

CREATE TABLE `t_ticket` (
    `id` int NOT NULL,
    `qr_code` longtext CHARACTER SET utf8mb4 NULL,
    `event_name` longtext CHARACTER SET utf8mb4 NULL,
    `UserId` int NOT NULL,
    `date` datetime(6) NOT NULL,
    CONSTRAINT `PK_t_ticket` PRIMARY KEY (`id`),
    CONSTRAINT `FK_t_ticket_t_user_id` FOREIGN KEY (`id`) REFERENCES `t_user` (`id`) ON DELETE CASCADE
) CHARACTER SET utf8mb4;

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20210729054955_Initial', '5.0.8');

COMMIT;

