START TRANSACTION;

CREATE TABLE `t_admin` (
    `id` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
    `login_date` datetime(6) NULL,
    `UserName` longtext CHARACTER SET utf8mb4 NULL,
    `NormalizedUserName` longtext CHARACTER SET utf8mb4 NULL,
    `login` longtext CHARACTER SET utf8mb4 NULL,
    `NormalizedEmail` longtext CHARACTER SET utf8mb4 NULL,
    `EmailConfirmed` tinyint(1) NOT NULL,
    `password` longtext CHARACTER SET utf8mb4 NULL,
    `SecurityStamp` longtext CHARACTER SET utf8mb4 NULL,
    `ConcurrencyStamp` longtext CHARACTER SET utf8mb4 NULL,
    `PhoneNumber` longtext CHARACTER SET utf8mb4 NULL,
    `PhoneNumberConfirmed` tinyint(1) NOT NULL,
    `TwoFactorEnabled` tinyint(1) NOT NULL,
    `LockoutEnd` datetime(6) NULL,
    `LockoutEnabled` tinyint(1) NOT NULL,
    `AccessFailedCount` int NOT NULL,
    CONSTRAINT `PK_t_admin` PRIMARY KEY (`id`)
) CHARACTER SET utf8mb4;

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20210731105037_AddAdminEntity', '5.0.8');

COMMIT;

