START TRANSACTION;

ALTER TABLE `t_event` ADD `ImageName` longtext CHARACTER SET utf8mb4 NULL;

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20210801060247_AddFieldImageName', '5.0.8');

COMMIT;

