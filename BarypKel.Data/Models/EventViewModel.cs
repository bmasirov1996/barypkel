﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarypKel.Data.Models
{
    public class EventViewModel
    {
        public string EventName { get; set; }
        public string Description { get; set; }
        public string Guests { get; set; }
        public DateTime EventDate { get; set; }
    }
}
