﻿using System;

namespace BarypKel.Data.Entities
{
    public class Ticket : Entity
    {         

        public string QRCodePath { get; set; }
        public int EventId { get; set; }
        public Event Event { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public DateTime EventDate { get; set; }         
    }
}
