﻿using System;
using System.Collections.Generic;

namespace BarypKel.Data.Entities
{
    public class Event : Entity
    {
        public string EventName { get; set; }
        public string Description { get; set; }
        public string Guests { get; set; }
        public string ImageName { get; set; }
        public List<Ticket> Tickets { get; set; }
        public DateTime EventDate { get; set; }
    }
}
