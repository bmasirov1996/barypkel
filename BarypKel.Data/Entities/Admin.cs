﻿using Microsoft.AspNetCore.Identity;
using System;

namespace BarypKel.Data.Entities
{
    public class Admin : IdentityUser
    {
        public DateTime? LastLoginDate { get; set; }
   

        public Admin()
        {
            this.LastLoginDate = DateTime.Now;
        }
    }
}
