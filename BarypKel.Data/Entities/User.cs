﻿using System;
using System.Collections.Generic;
 

namespace BarypKel.Data.Entities
{
    public class User : Entity
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public List<Ticket> Tickets { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
